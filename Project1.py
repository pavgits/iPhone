from datetime import datetime
user_input = input("Enter your goal and deadline to achieve seperated by colon \n")
input_list = user_input.split(":")  # Split function is to enable the LIST datatype

goal = input_list[0]
deadline = input_list[1]
deadline_date = datetime.strptime(deadline,"%d.%m.%Y") #To convert the deadline from string to date
## To calculate how many days from now till deadline
today_date = datetime.today()
time_till = deadline_date - today_date
hours_till = int(time_till.total_seconds() / 60 / 60) # deadline to dispaly in hours.Converted the output to int to avoid  decimals in output
#print(f"Time remaining till deadline to achjeve the goal {goal} are {time_till.days} days") # output in Days
print(f"Time remaining till deadline to achjeve the goal {goal} are {hours_till} hours") # output in hours
